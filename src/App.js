import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import SayHello from './lessons/SayHelloWithValidate';
import ConditionMessage from './lessons/ConditionalRenderComponent';
import RerenderTime from './lessons/RerenderTime';
import BoxStyle from './lessons/StyleReactComponent';
import EventSample from './lessons/EventHandlerSample';
import StopWatchControl from './lessons/StopWatchControl';
import DomRefs from './lessons/DomRefs';
import NameForm from './lessons/NameForm';
import ControllingFormValue from './lessons/ControllingFormValue';
import ComponentWithId from './lessons/ComponentWithId';
import UserInfoControl from './lessons/UserInfoControl';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        {/* <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p> */}
        <UserInfoControl />
        <ComponentWithId />
        <ControllingFormValue />
        <NameForm
          getErrorMessage={value => {
            if (value.length < 3) {
              return `Value must be at least 3 characters, but is only ${value.length}`
            }
            if (!value.includes('s')) {
              return `Value does not include "s", but it should!`
            }
            return null
          }}
        />
        <SayHello firstName="Pagorn" lastName="Phusaisakul" />
        <ConditionMessage message={'Pruxasin'} />
        <RerenderTime />
        <BoxStyle />
        <EventSample />
        <StopWatchControl />
        <DomRefs />

      </div>
    );
  }
}

