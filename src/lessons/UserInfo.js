import React from 'react'
import axios from 'axios'

export default class UserInfo extends React.Component {
    static TOKEN = '4bb0e237cc6058d7f36e41f298e92b80d50169e8'

    constructor(props) {
        super(props)

        this.state = {
            name: undefined,
            loaded: false,
        }
    }

    componentDidMount() {
        this.fetchUserByFetch()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.username !== this.props.username) {
            this.setState({
                loaded: false
            })
            this.fetchUserByFetch()
        }
    }

    fetchUserByFetch = () => {
        fetch('https://api.github.com/graphql',
            {
                method: 'POST',
                body: JSON.stringify({
                    query: `{
                        user(login: "${this.props.username}") {
                            name,
                            email,
                            bio,
                            url,
                            company,
                            location,
                        }
                    }`
                }),
                headers: { authorization: `bearer ${UserInfo.TOKEN}` },
            }
        ).then(response => {
            return response.json()
        }).then(parsedJSON => {
            return parsedJSON.data.user
        }).then((user) => {
            console.log(user)
            this.setState({
                loaded: true,
                user: user
            })
        }).catch(error => {
            this.setState({ error })
            return null
        })
    }

    fetchUser = () => {
        axios({
            url: 'https://api.github.com/graphql',
            method: 'post',
            data: {
                query: `{
                    user(login: "${this.props.username}") {
                        name,
                        email,
                        bio,
                        url,
                        company,
                        location,
                    }
                }`,
            },
            headers: { Authorization: `bearer ${UserInfo.TOKEN}` },
        }).then(response => {
            console.log(response.data)
            return response.data.data.user
        }).catch(error => {
            this.setState({ error })
            return null
        }).then((user) => {
            this.setState({
                loaded: true,
                user: user
            })
        })
    }

    render() {
        if (!this.state.loaded) { return "loading" }
        return this.props.children({ user: this.state.user })
    }
}

