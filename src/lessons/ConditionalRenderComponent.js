import React from 'react';

export default class ConditionMessage extends React.Component {

    render() {

        const { message } = this.props

        return (
            <div>
                {
                    message ?
                        <div>{message}</div>
                        : <div>No message</div>
                }
            </div>
        )
    }

}