import React from 'react'
import classnames from 'classnames'
import './Field.css'

class CommaInput extends React.Component {
    handleCommaSeparatedChange = event => {
        const { value } = event.target
        const allValues = value
            .split(',')
            .map(v => v.trim())
            .filter(Boolean)
        if (value[value.length - 1] === ',') {
            allValues.push(null)
        }
        this.props.onChange(allValues)
    }

    render() {
        let commaText = (this.props.value || []).join(',')
        return (
            <input
                value={commaText}
                type="text"
                onChange={this.handleCommaSeparatedChange}
            />
        )
    }
}

class MultilineInput extends React.Component {
    handleMultilineChange = event => {
        const { value } = event.target
        const allValues = value
            .split('\n')
            .map(v => v.trim())
            .filter(Boolean)
        if (value[value.length - 1] === '\n') {
            allValues.push(null)
        }
        this.props.onChange(allValues)
    }

    render() {
        let lineText = (this.props.value || []).join('\n')
        return (
            <textarea
                rows={MyFancyForm.availableOptions.length}
                value={lineText}
                onChange={this.handleMultilineChange}
            />
        )
    }
}

class MultiSelectList extends React.Component {
    handleOptionToggle = event => {
        const allVals = Array.from(
            event.target.selectedOptions,
        ).map(o => o.value)
        this.props.onChange(allVals)
    }

    render() {
        let allValue = this.props.allValue || []
        let selectedValues = this.props.selectedValues || []
        return (
            <select
                multiple
                size={allValue.length}
                value={selectedValues}
                onChange={this.handleOptionToggle}
            >
                {allValue.map(
                    optionValue => (
                        <option
                            key={optionValue}
                            value={optionValue}
                        >
                            {optionValue}
                        </option>
                    ),
                )}
            </select>
        )
    }
}


class Field extends React.Component {
    render() {
        let fieldClass = classnames(
            'Field',
            this.props.error && 'Field--error'
        )
        return (
            <div className={fieldClass}>
                <label className='Field__label'>
                    {this.props.title}
                </label>
                <div className='Field__control'>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default class MyFancyForm extends React.Component {
    static availableOptions = [
        'apple',
        'grape',
        'cherry',
        'orange',
        'pear',
        'peach',
    ]

    state = {
        selections: [],
    }

    handleSelectionsChange = (selections) => {
        this.setState({
            selections: selections
        })
    }

    render() {
        return (
            <div>
                <Field
                    title="comma separated values:"
                    children={<CommaInput
                        value={this.state.selections}
                        onChange={this.handleSelectionsChange}
                    />}
                />
                <Field
                    title="multiline values:"
                    children={<MultilineInput
                        value={this.state.selections}
                        onChange={this.handleSelectionsChange}
                    />}
                />
                <Field
                    title="multiSelect values:"
                    children={<MultiSelectList
                        allValue={MyFancyForm.availableOptions}
                        selectedValues={this.state.selections}
                        onChange={this.handleSelectionsChange}
                    />}
                />
            </div >
        )
    }
}