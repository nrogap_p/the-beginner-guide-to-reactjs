import React from 'react'

export default class ComponentWithId extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            items: [
                { id: 'a', value: 'apple' },
                { id: 'o', value: 'orange' },
                { id: 'g', value: 'grape' },
                { id: 'p', value: 'pear' },
            ]
        }
    }

    componentDidMount() {
        setInterval(this.randomizeItem, 1000)
    }

    randomizeItem = () => {
        this.setState({
            items: this.shuffle(this.state.items)
        })
    }

    render() {
        return (
            <div>
                <div>
                    <h1>With Key as Index</h1>
                    {this.state.items.map((item, index) => (
                        <input key={index} value={item.value} readOnly/>
                    ))}
                </div>
                <div>
                    <h1>With Key</h1>
                    {this.state.items.map(item => (
                        <input key={item.id} value={item.value} readOnly/>
                    ))}
                </div>

            </div>
        )
    }

    //อัลกอริทึมนี้จะสลับแค่ครั้งสองตำแหน่ง ถ้ามีสี่ตำแหน่ง n/2
    shuffle(array) {
        let currentIndex = array.length,
            temporaryValue,
            randomIndex

        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(
                Math.random() * currentIndex,
            )
            currentIndex -= 1
            // And swap it with the current element.
            temporaryValue = array[currentIndex]
            array[currentIndex] = array[randomIndex]
            array[randomIndex] = temporaryValue
        }
        return array
    }
}