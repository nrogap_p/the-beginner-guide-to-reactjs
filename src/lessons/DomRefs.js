import React from 'react';

import VanillaTilt from 'vanilla-tilt/lib/vanilla-tilt';
import './vanillaTiltStyle.css';

class Tilt extends React.Component {

    render() {
        return (
            <div
                ref={node => (this.rootNode = node)}
                className="tilt-root"
            >
                <div className="tilt-child">
                    <div className="totally-centered" {...this.props} />
                </div>
            </div>
        )
    }

    componentDidMount() {
        VanillaTilt.init(this.rootNode, {
            max: 25,
            speed: 400,
            glare: true,
            'max-glare': 0.5,
        })
    }
}

export default class DomRefs extends React.Component {
    render() {
        return (
            <div className="totally-centered">
                <Tilt>
                    <div className="totally-centered">
                        vanilla-tilt.js
                    </div>
                </Tilt>
            </div>
        )
    }

}
