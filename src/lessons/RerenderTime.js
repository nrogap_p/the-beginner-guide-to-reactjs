import React from 'react';

export default class RerenderTime extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            textTime: '',
        }
    }

    componentDidMount() {
         // ต้องส่ง context ด้วย bind ไม่งั้นจะไม่รู้จัก this.setState
        //  this.timer = setInterval(this.tickFunction.bind(this), 1000)

        // กรณีตั้ง tick เป็น arrow function
         this.timer = setInterval(this.tickArrow, 1000)
       
    }

    componentWillUnmount() {
        clearInterval(this.timer)
    }

    render() {
        return (
            <div>It is {this.state.textTime}</div>
        )
    }

    tickArrow = () => {
        this.setState({
            textTime: new Date().toLocaleTimeString(),
        })
    }

    tickFunction() {
        this.setState({
            textTime: new Date().toLocaleTimeString(),
        })
    }
}