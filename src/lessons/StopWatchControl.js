import React from 'react';

import StopWatch from './StopWatch';

export default class StopWatchControl extends React.Component {
    state = { showStopWatch: true }

    render() {
        const { showStopWatch } = this.state
        return (
            <div>
                <label>
                    Show Stop Watch{' '}
                    <input
                        type="checkbox"
                        checked={showStopWatch}
                        onChange={() =>
                            this.setState(s => ({
                                showStopWatch: !s.showStopWatch,
                            }))
                        }
                    />
                </label>
                <hr />
                {showStopWatch ? <StopWatch lapse={1000} isRunning={false} /> : null}
            </div>
        )
    }
}