import React from 'react'

import UserInfo from './UserInfo'

export default class UserCompanyControl extends React.Component {
    state = {
        username: undefined
    }

    handleSubmit = event => {
        event.preventDefault()
        this.setState({
            username: this.inputNode.value,
        })
    }

    render() {
        const { username } = this.state
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input ref={node => (this.inputNode = node)} />
                </form>
                {username ? (
                    <UserInfo username={username}>
                        {(props)=>{
                            return (<h1>{props.user ? props.user.name : 'error'}</h1>)
                        }}
                    </UserInfo>
                ) : null}
            </div>
        )
    }
}