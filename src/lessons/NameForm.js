import React from 'react';

export default class NameForm extends React.Component {
    // constructor(props) {
    //     super(props)
    //     this.state = {
    //         error: this.props.getErrorMessage('')
    //     }
    //     console.log(this.state.error)
    // }

    render() {
        const {error} = this.state || ''
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
                    <input
                        name="username"
                        type="text"
                        ref={node => (this.inputNode = node)}
                        onChange={this.handleChange}        
                    />
                </label>
                {error ? (
                    <div style={{ color: 'red' }}>
                        {error}
                    </div>)
                    : null
                }
                <button type="submit">SUBMIT</button>
            </form>
        )
    }

    handleChange = event => {
        const { value } = event.target
        this.setState({
            error: this.props.getErrorMessage(value),
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        // console.log({ target: event.target })
        // console.log(event.target[0].value)
        // console.log(event.target.elements.username.value)
        // console.log(this.inputNode.value)

        const value = event.target.elements.username.value
        const error = this.props.getErrorMessage(
            value,
        )

        if (error) {
            alert(`error: ${error}`)
        } else {
            alert(`success: ${value}`)
        }
    }
}