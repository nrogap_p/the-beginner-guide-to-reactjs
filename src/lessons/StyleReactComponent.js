import React from 'react';

import './box.css';

export default class StyleReactComponent extends React.Component {
    render() {
        return (
            <div>
                <div
                    className="box large">
                    My Box
                </div>
                <BoxByProps
                    size="medium"
                    style={{ backgroundColor: 'lightblue' }}
                >
                    His Box
                </BoxByProps>
            </div>
        );
    }
}

class BoxByProps extends React.Component {
    render() {
        const {className, size, style, ...rest } = this.props

        const boxStyleString = `box ${className} ${size}`

        return (
            <div
                className={boxStyleString}
                style={{ paddingLeft: 20, ...style }}
                {...rest}
            >
            </div>
        );
    }
}

