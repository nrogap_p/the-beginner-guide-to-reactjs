import React from 'react';
import './stopwatch.css';

export default class StopWatch extends React.Component {
    constructor(props) {
        super(props)

        const { lapse, isRunning } = props

        this.state = {
            lapse: lapse || 0,
            isRunning: isRunning || false,
        }
    }

    render() {
        const { lapse, isRunning } = this.state

        return (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <label className="labelStyle"> {lapse} ms </label>
                </div>
                <button onClick={this.runLapse} className="buttonStyle">
                    {isRunning ? 'PAUSE' : 'START'}
                </button>
                <button onClick={this.clearLapse} className="buttonStyle">
                    CLEAR
                </button>
            </div>
        )
    }

    componentDidMount() {
        if(this.state.isRunning){
            this.startLapse()
        }
    }

    componentWillUnmount() {
        clearInterval(this.time)
    }

    runLapse = () => {

        this.setState(state => {
            if (state.isRunning) {
                clearInterval(this.timer)
            } else {
               this.startLapse()
            }

            return { isRunning: !state.isRunning }
        })
    }

    startLapse = () => {
        const startTime = Date.now() - this.state.lapse
        this.timer = setInterval(() => {
            this.setState({
                lapse: Date.now() - startTime,
            })
        })
    }

    clearLapse = () => {
        clearInterval(this.timer)
        this.setState({
            lapse: 0,
            isRunning: false,
        })
    }

}
