import React from 'react';

export default class MyFancyForm extends React.Component {
    static availableOptions = [
        'apple',
        'grape',
        'cherry',
        'orange',
        'pear',
        'peach',
    ]

    state = {
        multiline: '',
        commaSeparated: '',
        multiSelect: [],
    }

    handleCommaSeparatedChange = event => {
        const { value } = event.target
        const allValues = value
            .split(',')
            .map(v => v.trim())
            .filter(Boolean)
        this.setState({
            commaSeparated: value,
            multiline: allValues.join('\n'),
            multiSelect: allValues.filter(v =>
                MyFancyForm.availableOptions.includes(v)
            ),
        })
    }

    handleMultilineChange = event => {
        const { value } = event.target
        const allValues = value
            .split('\n')
            .map(v => v.trim())
            .filter(Boolean)
        this.setState({
            multiline: value,
            commaSeparated: allValues.join(','),
            multiSelect: allValues.filter(v =>
                MyFancyForm.availableOptions.includes(v)
            ),
        })
    }

    handleMultiSelectChange = event => {

    }



    render() {
        const { multiline, multiSelect } = this.state
        return (
            <div>
                <div>
                    <label>
                        comma separated values:
                    <input
                            type="text"
                            onChange={
                                this.handleCommaSeparatedChange
                            }
                        />

                    </label>
                </div>
                <div>
                    <label>
                        multiline values:
                    <textarea
                            rows={MyFancyForm.availableOptions.length}
                            value={multiline}
                            onChange={
                                this.handleMultilineChange
                            }
                        />
                    </label>
                </div>
                <div>
                    <label>
                        multiSelect values:
                        <select
                            multiple
                            size={MyFancyForm.availableOptions.length}
                            value={multiSelect}
                            onChange={
                                this.handleMultiSelectChange
                            }
                        >
                            {MyFancyForm.availableOptions.map(
                                optionValue => (
                                    <option
                                        key={optionValue}
                                        value={optionValue}
                                    >
                                        {optionValue}
                                    </option>
                                ),
                            )}



                        </select>
                    </label>
                </div>
            </div>
        )
    }
}