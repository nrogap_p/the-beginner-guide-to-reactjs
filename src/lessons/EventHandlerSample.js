import React from 'react';

export default class EventHandlerSample extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            eventCount: 0, username: ''
        }
    }

    render() {
        return (
            <div>
                <p>
                    There have been {this.state.eventCount} events
                </p>
                <p>
                    <button onClick={this.increment}>⚛</button>
                </p>
                <p>
                    You typed: {this.state.username}
                </p>
                <p>
                    <input onChange={this.updateUsername} />
                </p>
            </div>
        )
    }

    increment = () => {
        this.setState({
            eventCount: this.state.eventCount + 1,
        })
    }

    updateUsername = (event) => {
        this.setState({
            username: event.target.value,
        })
    }

    
}